from flask import Flask, request, jsonify
from Zfs import Zfs

import logging
import logging.config
import os
import json
import subprocess

app = Flask(__name__)
app.config['STATIC_FOLDER'] = 'app'

# Defaults
app.config["ROOT_VOLUME"] = "pool/volumes"

config_filename = os.path.join(app.instance_path, 'dpzfs.cfg')
if os.path.exists(config_filename):
    app.config.from_pyfile(config_filename)

app.config.from_envvar('DPZFS_SETTINGS', silent=True)

handler = logging.FileHandler(app.config['LOGGING_LOCATION'])
handler.setLevel(app.config['LOGGING_LEVEL'])
formatter = logging.Formatter(app.config['LOGGING_FORMAT'])
handler.setFormatter(formatter)
app.logger.addHandler(handler)

app.logger.warn("Starting Docker volume plugin for zfs")

zfs = Zfs(app.logger, app.config['ROOT_VOLUME'])


@app.route('/Plugin.Activate', methods=['POST'])
def plugin_activate():
    app.logger.warn("Active plugin-zfs plugin")
    app.logger.warn("Received: " + str(request.data))

    return jsonify({"Implements": ["VolumeDriver"]})


@app.route('/VolumeDriver.Create', methods=['POST'])
def plugin_create():
    app.logger.warn("Received: " + str(request.data))
    data = json.loads(request.data.decode('UTF-8'))

    name = data['Name']

    response = {"Err": None}
    if name == '':
        response['Err'] = "Empty volume name"
    else:
        try:
            zfs.create(name)
        except subprocess.CalledProcessError as ex:
            response['Err'] = ex.output.decode('UTF-8')

    return jsonify(response)


@app.route('/VolumeDriver.Remove', methods=['POST'])
def plugin_remove():
    app.logger.warn("Received: " + str(request.data))
    data = json.loads(request.data.decode('UTF-8'))

    name = data['Name']

    response = {"Err": None}
    if name == '':
        response['Err'] = "Empty volume name"
    else:
        try:
            zfs.remove(name)
        except subprocess.CalledProcessError as ex:
            response['Err'] = ex.output.decode('UTF-8')

    return jsonify(response)


@app.route('/VolumeDriver.Mount', methods=['POST'])
def plugin_mount():
    data = json.loads(request.data.decode('UTF-8'))

    name = data['Name']
    volume = zfs.get_volume(name)

    if volume is None:
        return jsonify({"Err": 'Error getting volume'})
    else:
        return jsonify({"Err": None, "Mountpoint": volume.mountpoint})


@app.route('/VolumeDriver.Get', methods=['POST'])
def plugin_get():
    data = json.loads(request.data.decode('UTF-8'))

    name = data['Name']
    volume = zfs.get_volume(name)

    if volume is None:
        return jsonify({"Err": 'Error getting volume'})
    else:
        return jsonify({"Err": None, "Volume": {"Name": volume.name, "Mountpoint": volume.mountpoint}})


@app.route('/VolumeDriver.Path', methods=['POST'])
def plugin_path():
    return plugin_mount()


@app.route('/VolumeDriver.List', methods=['POST'])
def plugin_list():
    volumes = zfs.list()

    vols = []

    for name in volumes:

        volume = volumes[name]
        vols.append({'Name': volume.name, 'Mountpoint': volume.mountpoint})

    return jsonify({"Err": None, 'Volumes': vols})


@app.route('/VolumeDriver.Unmount', methods=['POST'])
def plugin_unmount():
    return jsonify({"Err": None})


@app.route('/info', methods=['GET'])
def info():
    info = subprocess.check_output(["zfs", "list"])
    return jsonify({"Err": None, "Info": info})


if __name__ == '__main__':
    debug = False
    if 'GODSTAT_ENV' in os.environ and \
                    os.environ['GODSTAT_ENV'] == 'dev':
        debug = True
    app.run(host='127.0.0.1', debug=debug)
