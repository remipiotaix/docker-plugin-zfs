import re
from subprocess import check_output, CalledProcessError


class ZfsError(Exception):
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return "ZfsError(" + self.message + ")"


class ZfsVolume:
    def __init__(self, base, name, mountpoint):
        self._base = base
        self._name = name
        self._mountpoint = mountpoint

    @property
    def mountpoint(self):
        return self._mountpoint

    @property
    def name(self):
        return self._name

    @property
    def base(self):
        return self._base

    def fullname(self):
        fn = self.base
        if self.name != '':
            fn += '/' + self.name

        return fn


class Zfs:
    def __init__(self, logger, rootfs):
        self.logger = logger
        self.rootfs = rootfs
        self.list_re = re.compile('^(' + rootfs + '/[^\s]+)\s+([^\s]+)$')

    def create(self, fs):
        """
        :param fs:
        :return: True if ZFS volume was created, False if the volume already exists
        :raise CalledProcessError if command fails
        """
        vol = self.get_volume(fs)
        if vol is None:
            check_output(["zfs", "create", self.rootfs + '/' + fs])
            return True
        else:
            return False

    def remove(self, fs):
        """
        :param fs:
        :return: True if ZFS volume was destroyed, False if the volume don't exist
        :raise CalledProcessError if command fails
        """
        vol = self.get_volume(fs)

        if vol is None:
            return False

        check_output(["zfs", "destroy", vol.fullname()])
        return True

    def list(self):
        volumes = dict()

        try:
            out = check_output(['zfs', 'list', self.rootfs, '-rHo', 'name,mountpoint'])

            for line in out.decode('UTF-8').split('\n'):
                m = self.list_re.match(line)
                if m is not None:
                    name = m.group(1)
                    mountpoint = m.group(2)
                    volumes[name] = ZfsVolume(self.rootfs, name.replace(self.rootfs + '/', '', 1), mountpoint)

            return volumes
        except CalledProcessError:
            return volumes

    def get_volume(self, fs):
        return self._get_full_name(self.rootfs + '/' + fs)

    def _get_root(self):
        return self._get_full_name(self.rootfs)

    def _get_full_name(self, name):
        getre = re.compile('^' + name + '\s+([^\s]+)$')

        try:
            out = check_output(['zfs', 'list', name, '-Ho', 'name,mountpoint'])

            m = getre.match(out.decode('UTF-8'))
            if m is None:
                return False

            mountpoint = m.group(1)
            if name == self.rootfs:
                name = ""
            else:
                name = name.replace(self.rootfs + '/', '')

            return ZfsVolume(self.rootfs, name, mountpoint)

        except CalledProcessError:
            return None
